# README #

This README explains how and what you need to run the program.

** The code is available in the src folder. **

### What is this repository for? ###

This repository helped us to:

* Synchronize the work
* Share the files and documents
* Version the project and keep older versions available

### How do I get set up? ###

You need to:

* Install R on your machine (https://www.r-project.org/)
* Install the additional R packages in the script
* Download the dataset "Soybean.rda" by "mlbench" from the directory
* Run the script "Projet.R"
* Check the documents in the doc folder

### Authors ###

* **Elie ABI HANNA DAHER**
* **Bilal EL CHAMI**

### Sources ###

mlbench (https://cran.r-project.org/web/packages/mlbench/index.html)